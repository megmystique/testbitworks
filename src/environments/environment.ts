// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAYnI3DHNXOq7Hqd8EAx6uMsn06Q47_WQk',
    authDomain: 'testproject-1dd5c.firebaseapp.com',
    databaseURL: 'https://testproject-1dd5c.firebaseio.com',
    projectId: 'testproject-1dd5c',
    storageBucket: '',
    messagingSenderId: '363991623731'
  }
};
