import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularFirestore} from 'angularfire2/firestore';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {MatDialog} from '@angular/material';
import {PopupComponent} from '../popup/popup.component';

export interface Todo {
  title: string;
  done: boolean;
}

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {
  id: string;
  todoUpdated: Array<Todo> = [];
  wasChanged = false;
  todos: Array<Todo> = [];
  edit = false;
  newTodo: Todo = {title: '', done: false};
  loading = false;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private route: ActivatedRoute, private afs: AngularFirestore, public dialog: MatDialog) {
    this.route.params.takeUntil(this.ngUnsubscribe).subscribe(params => {
      this.id = params['id'];
    });
  }

  ngOnInit() {
    this.loading = true;
    if (this.id) {
      this.afs.collection(`todo`).doc(this.id).valueChanges().takeUntil(this.ngUnsubscribe).subscribe(res => {
        if (res) {
          if (!this.edit) {
            this.todos = res['todo'];
            this.loading = false;
          } else {
            this.todoUpdated = res['todo'];
            this.wasChanged = true;
          }
        } else {
          this.loading = false;
        }
      }, () => this.loading = false);
    }
  }

  add() {
    if (this.newTodo.title.length) {
      this.todos.push(this.newTodo);
      this.afs.collection('todo').doc(this.id).set({todo: this.todos});
      this.newTodo = {title: '', done: false};
    }
  }

  update() {
    if (this.wasChanged) {
      this.openDialog();
    } else {
      this.afs.collection('todo').doc(this.id).set({todo: this.todos});
      this.edit = false;
    }

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PopupComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().takeUntil(this.ngUnsubscribe).subscribe(result => {
      if (result === 'server') {
        this.todos = this.todoUpdated;
        this.edit = false;
        this.wasChanged = false;
      } else if (result === 'client') {
        this.afs.collection('todo').doc(this.id).set({todo: this.todos});
        this.edit = false;
        this.wasChanged = false;
      }
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
