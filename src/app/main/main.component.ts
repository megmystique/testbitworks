import {Component, OnInit} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  id = null;

  constructor(private db: AngularFirestore, private router: Router) {
  }

  ngOnInit() {
    this.id = this.db.createId();
    if (this.id) {
      this.router.navigate([this.id]);
    }
  }

}
